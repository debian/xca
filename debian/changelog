xca (2.8.0-1) unstable; urgency=medium

  * Use Qt6 instead of Qt5.  Files changed:
    - d/control: Dependencies
  * New upstream release (2.8.0)

 -- Thomas Ward <teward@ubuntu.com>  Mon, 7 Jul 2024 16:30:00 -0400

xca (2.6.0-2) unstable; urgency=medium

  * d/control: Add 'or' dependency on libqt5sql5 for backports compat
  * d/clean: Add additional paths for dh_clean to clean out - some files 
    are generated during build time and not in Makefile clean targets, so
    clean it here. Allows double-builds to work. (Closes: #1045831)
  * d/control: Remove libqt5sql5 dependency as it should be included in 
    libqt5sql5{,t64}. (Closes: #1074383) 

 -- Thomas Ward <teward@ubuntu.com>  Fri, 27 Jun 2024 17:13:00 -0400

xca (2.6.0-1) unstable; urgency=medium

  * New upstream release (2.6.0)
  * d/control: Remove constraints unnecessary since Buster:
    - Build-Depends: Drop versioned constraint on dpkg-dev.
    - Build-Depends: Use pkgconf instead of pkg-config; add pkg-config
      as an "OR"'d dependency for systems without pkgconf (allows for 
      easier backport compat)
  * d/control: Update dependencies affected by t64 migration
    (Closes: #1067697)
  * d/source/git-patches: Remove now-obsolete git-patches. 
  * d/copyright: Update copyright stanzas.

 -- Thomas Ward <teward@ubuntu.com>  Fri, 5 Apr 2024 17:14:00 -0400

xca (2.5.0-1) unstable; urgency=medium

  * New upstream release (2.5.0)
  * Upstream now uses CMake for builds, which invalidates multiple quilt 
    patches.
  * Dropped patches from d/patches:
    - xca-240-ossl3.patch: Already included in 2.5.0
    - 0001-Remove-misc-Info.plist-in-clean-target.patch: Obsolete due 
      to CMake move.
    - 0002-pkg-config-remove-hardcoding.patch: Obsolete due to
      CMake move.
  * d/control: Update build dependencies because we use CMake; use XCA
    upstream git home for build-dependencies list.
  * d/rules: Numerous changes to adapt for CMake build environment 
    as changed by Upstream.

 -- Thomas Ward <teward@ubuntu.com>  Wed, 11 Oct 2023 14:52:56 -0400

xca (2.4.0-2) unstable; urgency=medium

  * New patches added:
    - d/patches/xca-240-ossl3.patch: Upstream OpenSSL 3.0 compatibility
      patches to allow builds and compat with OpenSSL 3.0.

 -- Thomas Ward <teward@ubuntu.com>  Fri, 11 Feb 2022 16:17:45 -0500

xca (2.4.0-1) unstable; urgency=medium

  * New upstream release (2.4.0)
  * Additional changes:
    - Drop d/patches/0003-fix-autoheader.patch - this is already fixed 
      in 2.4.0.
    - d/control: Add qttools5-dev package to build dependencies.
    - d/xca-dbgsym.lintian-overrides: Suppress a warning about ELF 
      decoder in Lintian - seen in sbuild tests against Unstable.

 -- Thomas Ward <teward@ubuntu.com>  Fri, 10 Dec 2021 19:23:24 -0500

xca (2.3.0-1) unstable; urgency=medium

  * New upstream release (2.3.0)
  * This upload will also fix the following Debian BTS Bugs:
    - "xca: OLD LN differs warning popups at startup" (Closes: #958886)
    - "xca: New upstream version 2.3.0 available" (Closes: #960580)
  * Additional changes:
    - d/patches/0003-fix-autoheader.patch: Apply upstream (pending) fixes 
      to handle issue with autoheader changes and build failures.
    - debian/control: Replace Recommends postgresql dependency to
      libqt5sql5-psql

 -- Thomas Ward <teward@ubuntu.com>  Mon, 27 Apr 2020 09:53:56 -0400

xca (2.2.1-1) unstable; urgency=medium

  * New upstream release (2.2.1).
  * d/control: Update Standards-Version to 4.5.0.
  * d/patches/0001-Remove-misc-Info.plist-in-clean-target.patch: Refresh patch
  * d/patches/0002-pkg-config-remove-hardcoding.patch: Refresh patch

 -- Thomas Ward <teward@ubuntu.com>  Mon, 03 Feb 2020 19:50:58 -0500

xca (2.1.2-3) unstable; urgency=medium

  * Fix FTCBFS: Export QT_SELECT. (Closes: #947481)
    Thanks to Helmut Grohne for patch.

 -- Thomas Ward <teward@ubuntu.com>  Fri, 27 Dec 2019 10:29:47 -0500

xca (2.1.2-2) unstable; urgency=medium

  * d/patches/0002-pkg-config-remove-hardcoding.patch: Remove hard-coded
    pkg-config in configure.ac, and replace it with $PKG_CONFIG references
    instead.
    Thanks to Helmut Grohne for the 1.4.1-1 patch, which is the basis
    for this patch.
    (Closes: #896891)

 -- Thomas Ward <teward@ubuntu.com>  Tue, 03 Sep 2019 16:27:02 -0400

xca (2.1.2-1) unstable; urgency=medium

  * New upstream version. (Closes: #927233)
  * d/watch: Update watch file to track upstream xca repository on Github.
  * d/compat: Update Debhelper compatibility to latest (12)
  * d/control:
    - New maintainer (Closes #931806)
    - Update Standards-Version to 4.4.0
    - Update Homepage to proper Upstream URL
    - Update Vcs-Git to point at Debian Salsa. Package now uses a
      git-buildpackage (gbp) workflow.
    - Add Vcs-Browser field.
    - Reorganize Build-Depends to be more readable.
    - Remove dh-autoreconf build depends.
    - Update debhelper build depends (compat is 12, so use debhelper >= 12)
    - Make requisite changes to adjust xca built by the packages to enable
      Remote DB support in xca: (Closes: #928678)
      - Add libqt5sql5 Build-Depends for Remote DB support.
      - Add Recommends on libqt5sql5-{mysql,postgresql} to xca package.
  * d/patches/0001-Remove-misc-Info.plist-in-clean-target.patch: Refresh
    patch to remove fuzz.
  * d/copyright: Update copyright file to be Machine-readable per
    https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

 -- Thomas Ward <teward@ubuntu.com>  Sat, 13 Jul 2019 18:17:10 -0400

xca (2.0.1-1) unstable; urgency=medium

  * New upstream release
  * XCA 2.0 depends on libqt5sql5-sqlite
  * Add NEWS file to describe database changes in the 2.0 release
  * Correct .orig.tar.gz generation, the filename was wrong

 -- Tino Mettler <tino+debian@tikei.de>  Tue, 03 Jul 2018 16:03:35 +0200

xca (1.4.1-1) unstable; urgency=medium

  * New upstream release

 -- Tino Mettler <tino+debian@tikei.de>  Wed, 21 Mar 2018 22:04:11 +0100

xca (1.4~pre3-1) unstable; urgency=medium

  [ Tino Mettler ]
  * New upstream git snapshot from commit ead8320a
  * As XCA does not use QT4 anymore, mention just QT in the short description
    (Closes: #882471)

  [ Christian Hohnstaedt ]
  * Of course we support Big Endian machines. Remove debugging mechanism
    (Closes: #882470)

 -- Tino Mettler <tino+debian@tikei.de>  Thu, 23 Nov 2017 13:35:21 +0100

xca (1.4~pre2-1) unstable; urgency=medium

  * New upstream git snapshot from commit 88677279
  * Mention new upstream version in 1.4~pre1-1 changelog entry
  * Remove 0001-Fix-further-spelling-errors-found-by-lintian-packge-.patch,
    applied upstream

 -- Tino Mettler <tino+debian@tikei.de>  Wed, 22 Nov 2017 14:02:03 +0100

xca (1.4~pre1-1) unstable; urgency=medium

  * New upstream git snapshot from commit 4ef4c9ad
  * Switch to OpenSSL 1.1 (Closes: #859826)
  * Add 0001-Fix-further-spelling-errors-found-by-lintian-packge-.patch
    to fix new spelling errors found by lintian
  * Bump standards version, no changes needed

 -- Tino Mettler <tino+debian@tikei.de>  Mon, 20 Nov 2017 21:28:27 +0100

xca (1.3.2-3) unstable; urgency=medium

  * Fix further spelling errors found by lintian packge checker
  * Change build depends from QT4 to QT5 (Closes: #875237)
  * Use changelog info from pkg-info.mk to fix lintian warning
  * Remove dh_make references from debian/watch to fix a lintian warning
  * Bump standards version, no changes needed

 -- Tino Mettler <tino+debian@tikei.de>  Tue, 19 Sep 2017 14:45:04 +0200

xca (1.3.2-2) unstable; urgency=medium

  * Enable all hardening flags, not just the default
  * Drop override for clean, it isn't needed anymore
  * Use dh-autoreconf to fix build failure when built twice in a row
  * Change Vcs-Git to the repository used for packaging, not upstream
  * Bump standards version, no changes needed
  * Add 0002-Fix-clean-target.patch to remove all generated files during clean
  * Add 0003-Allow-to-specify-CPPFLAGS-on-the-command-line.patch to use
    the CPPFLAGS environment variable
  * Drop 0001-Use-Debian-specific-CPPFLAGS.patch which is replaced by the obove
    patch
  * Build-Depend on libssl1.0-dev to fix FTBFS with OpenSSL 1.1
    (Closes: #828604)
  * Don't strip xca, the xca-dbgsym package needs the debug symbols

 -- Tino Mettler <tino+debian@tikei.de>  Wed, 09 Nov 2016 14:53:14 +0100

xca (1.3.2-1) unstable; urgency=medium

  * New upstream release (Closes: #706539, #811347)
  * Switch to dh build
  * Remove menu file, according to #741573
  * Bump standards version to 3.9.6, no changes needed
    Thanks to Chris Knadle for double checking.
  * Add 0001-Use-Debian-specific-CPPFLAGS.patch to use hardening flags
    Thanks to Chris Knadle for the suggestion. This also replaces the
    change from the previous NMU.
  * Add 0002-Fix-spelling-errors-found-by-lintian-packge-checker.patch
    provided by Chris Knadle
  * Drop 0001-Fix-undefined-reference-to-symbol-__cxa_free_excepti.patch
    as upstream now uses g++ for linking
  * Drop 0002-RedHat-Bug-1164340-segfault-when-viewing-a-RHEL-enti.patch
    which was included upstream

 -- Tino Mettler <tino+debian@tikei.de>  Mon, 01 Feb 2016 13:42:22 +0100

xca (1.0.0-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/rules:
    - Quote CF variable contents to fix FTBFS bug (Closes: #809056)

 -- Christopher Knadle <Chris.Knadle@coredump.us>  Mon, 04 Jan 2016 01:49:40 -0500

xca (1.0.0-2) unstable; urgency=medium

  * RedHat Bug #1164340 - segfault when viewing a RHEL entitlement certificate
    (Closes: #770091)

 -- Tino Mettler <tino+debian@tikei.de>  Tue, 18 Nov 2014 22:09:55 +0100

xca (1.0.0-1) unstable; urgency=medium

  * New upstream release (Closes: #758440)
  * Fix undefined reference to symbol '__cxa_free_exception@@CXXABI_1.3'
    Thanks to YunQiang Su <wzssyqa@gmail.com> (Closes: #713672, #762419)
  * Show all build commands during build to fix a lintian warning
  * Use DEB_REF instead of UPSTREAM_REF for git based patch creation
  * Update maintainer name

 -- Tino Mettler <tino+debian@tikei.de>  Thu, 23 Oct 2014 11:59:26 +0200

xca (0.9.3-1) unstable; urgency=low

  * new upstream version
  * remove patch to search in multiarch library path, upstream now uses
    pkg-config
  * add pkg-config to Build-Depends
  * build with hardening flags enabled
  * patch configure script to preserve $CPPFLAGS and $LDFLAGS
  * bump standards version to 3.9.3, no changes needed

 -- Tino Keitel <tino+debian@tikei.de>  Sat, 02 Jun 2012 22:15:06 +0200

xca (0.9.1-1) unstable; urgency=low

  * New upstream version (Closes: #650367)

 -- Tino Keitel <tino+debian@tikei.de>  Sat, 03 Dec 2011 12:27:04 +0100

xca (0.9.1~git7ffc768-2) unstable; urgency=low

  * fix configure script to handle multiarch libraries (Closes: #642823)

 -- Tino Keitel <tino+debian@tikei.de>  Tue, 25 Oct 2011 22:04:33 +0200

xca (0.9.1~git7ffc768-1) unstable; urgency=low

  * build from release.0.9.x upstream branch (Closes: #605141, #615796)
  * add build-indep and build-arch build targets to please lintian
  * bump standards version to 3.9.2

 -- Tino Keitel <tino+debian@tikei.de>  Mon, 04 Jul 2011 10:51:20 +0200

xca (0.9.0-1) unstable; urgency=low

  * new upstream version
  * bump standards version to 3.9.1
  * remove patches, they are included in the upstream source:
    - add_32x32_icon-0.8.1
    - manpage_typo-0.8.1

 -- Tino Keitel <tino+debian@tikei.de>  Thu, 30 Sep 2010 11:45:15 +0200

xca (0.8.1-1) unstable; urgency=low

  * new upstream version 0.8.1
  * change source format to 3.0 (quilt)
  * add ${misc:Depends}, found by lintian
  * add 32x32 pixel sized menu icon, to please lintian
  * added patch to fix a manpage typo, found by lintian

 -- Tino Keitel <tino+debian@tikei.de>  Sun, 10 Jan 2010 22:17:39 +0100

xca (0.8.0-1) unstable; urgency=low

  * new upstream version (Closes: #560509)
  * add libltdl-dev build-dep, required by xca 0.8.0
  * provide a directory for the documentation in the configure call
  * actually invoke the configure target
  * correctly declare the installation path
  * no need to install HTML documentation via dh_installdocs, make install
    does it
  * no need to remove misplaced HTML documentation anymore
  * use the generated Local.mak instead of a separate configure-stamp

 -- Tino Keitel <tino+debian@tikei.de>  Tue, 15 Dec 2009 23:01:45 +0100

xca (0.7.0-1) unstable; urgency=low

  * New upstream version

 -- Tino Keitel <tino+debian@tikei.de>  Mon, 14 Sep 2009 20:55:44 +0200

xca (0.6.4-35-g6c2d6a6-1) UNRELEASED; urgency=low

  * Initial release. (Closes: #519003)

 -- Tino Keitel <tino+debian@tikei.de>  Fri, 06 Mar 2009 20:58:46 +0100
